<?php

namespace Drupal\Tests\video_embed_twitter\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\video_embed_twitter\Plugin\video_embed_field\Provider\Twitter;

/**
 * Test that URL parsing for the provider is functioning.
 *
 * @group video_embed_twitter
 */
class ProviderUrlParseTest extends UnitTestCase {

  /**
   * @dataProvider urlsWithExpectedIds
   *
   * Test URL parsing works as expected.
   */
  public function testUrlParsing($url, $expected) {
    $this->assertEquals($expected, Twitter::getIdFromInput($url));
  }

  /**
   * A data provider for URL parsing test cases.
   *
   * @return array
   *   An array of test cases.
   */
  public function urlsWithExpectedIds() {
    return [
      [
        'https://twitter.com/i/status/1545224555962277888',
        '1545224555962277888',
      ],
      [
        'https://twitter.com/i/status/1545224555962277888',
        FALSE,
      ],
    ];
  }

}
