<?php

namespace Drupal\video_embed_twitter\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * @VideoEmbedProvider(
 *   id = "twitter",
 *   title = @Translation("Twitter")
 * )
 */
class Twitter extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    // @todo , consider using the JavaScript version, however iframes are less
    // impact to page load and also don't grant JS access to your website to
    // Twitter.
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'src' => sprintf('https://twitter.com/i/videos/tweet/%s', $this->getVideoId(), $autoplay),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $re = '/^https?:\/\/?twitter.com\/(i)\/(status)\/(?<id>[0-9]*)$/';
    preg_match($re, $input, $matches, PREG_OFFSET_CAPTURE, 0);
    // Print the entire match result.
    return $matches['id'][0] ?? FALSE;
  }

}
